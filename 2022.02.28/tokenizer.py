import re

def sentences(p):
    groups = re.findall(r'(\\p{Lu}[^.]+\.)', p)
    print(groups)
    exit(0)

with open("somatropina.txt", "r") as fh:
    content = fh.read()


content = re.sub(r'<[^>]+>', "", content)
paragraphs = [
    re.sub(r'\s+', " ", x.strip())
    for x in re.split(r'\s*\n\s*\n+\s*', content) if len(x) > 0]

paragraphs = [sentences(x) for x in paragraphs]

print(paragraphs)

