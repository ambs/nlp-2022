import stanza
import re

stanza.download('pt')
with open("somatropina.txt", "r") as fh:
    content = fh.read()

content = re.sub(r'<[^>]+>', "", content)

nlp = stanza.Pipeline(lang='pt', processors='tokenize,mwt,pos,lemma')  # , use_gpu=False)
doc = nlp(content)

for i, sentence in enumerate(doc.sentences, start=1):
    print(f'====== Sentence {i} tokens =======')
    print(*[f'word: {word.text}\tupos: {word.upos}\tlema: {word.lemma}\tfeats: {word.feats if word.feats else "_"}' for
            sent in doc.sentences for word in sent.words], sep='\n')
