# hearst_patterns.py

from utils import load_conll_words

text = load_conll_words("leme.conll", just_words=False)

def search_hearst_pattern(pattern, text):
    patt_len = len(pattern)
    matches = []
    for s in text:
        for i in range(0, len(s) - patt_len + 1):
            seq = s[i:i+patt_len]
            match = {}
            for patt_token, sent_token in zip(pattern, seq):
                if patt_token[0] == '?':  # variable
                    if ':' in patt_token:
                        (_, pos) = patt_token.split(":")
                        if pos == sent_token[2]:
                            match[patt_token] = sent_token[0]
                        else:
                            break
                    else:
                        match[patt_token] = sent_token[0]
                else:
                    if patt_token != sent_token[0]:
                        break
            else:
                matches.append(match)
    return matches


hits = search_hearst_pattern(["dor", "de", "?X:NOUN", "ou", "?Z:NOUN"], text)
print(hits)


