# lang_model.py
import math
import re
import random
from utils import load_conll_words, load_word_set


class LanguageModel:
    def __init__(self, filename):
        self.bigrams_count = 0
        self.bigrams_voc_size = 0
        self.bigrams = {}       # prob. bigrams
        self.words = {}         # prob. words
        self.vocabulary = {"<UNK>", "<NUM>"}

        text = load_conll_words(filename, lowercase=True)

        self.corpus_size = self.get_corpus_size(text)
        self.words = self.get_word_list(text, 100)  # dic:  word => #
        self.vocabulary = self.vocabulary.union(set(self.words.keys()))
        self.vocabulary_size = len(self.vocabulary)
        self.smoothed_word_frequencies()
        self.bigrams = self.compute_bigrams(text)

    def seq_probability(self, sentence):
        prob = 0
        for i in range(0, len(sentence) - 1):    # - N + 1
            p = self.pair_probability(*sentence[i:i+2])  # N = 2
            prob += math.log(p)
        return - prob

    def pair_probability(self, w1, w2):
        bigram = f"{w1}_{w2}"
        numer = self.bigrams.get(bigram, 1 / (self.bigrams_count + self.bigrams_voc_size))
        w1 = self.valid(w1)
        denom = self.words.get(w1, 1 / (self.corpus_size + self.vocabulary_size))
        return numer / denom

    def valid(self, word):
        if word not in self.vocabulary:
            if re.fullmatch(r'\d+([.,]\d+)?', word):
                return '<NUM>'
            else:
                return '<UNK>'
        else:
            return word

    def compute_bigrams(self, text):
        freqs = {}
        count = 0
        N = 2
        # count bigrams
        for sent in text:
            n = len(sent) - N + 1
            for i in range(0, n):
                seq = map(self.valid, sent[i:i+N])
                seq = "_".join(seq)
                freqs[seq] = freqs.get(seq, 0) + 1
                count += 1
        # compute probabilities
        for bigram in freqs:
            freqs[bigram] = freqs[bigram] / count
        self.bigrams_count = count
        self.bigrams_voc_size = len(freqs.keys())
        return freqs

    def smoothed_word_frequencies(self):
        for w in self.words:
            self.words[w] = (self.words[w] + 1) / (self.corpus_size + self.vocabulary_size)

    def get_corpus_size(self, text):
        size = 0
        for s in text:
            size += len(s)
        return size

    def get_word_list(self, text, limit):
        freqs = {}
        for s in text:
            for w in s:
                if re.fullmatch(r'\d+([.,]\d+)?', w):
                    w = '<NUM>'
                freqs[w] = freqs.get(w, 0) + 1
        # remove low occurring words, and add them as unknown
        freqs['<UNK>'] = 0
        for w in [x for x in freqs]:
            if freqs[w] < limit:
                freqs['<UNK>'] += freqs[w]
                del(freqs[w])
        return freqs

    def next_word(self, w1):
        pairs = {}
        for bigram in self.bigrams.keys():
            match = re.fullmatch(f"{re.escape(w1)}_(.+)", bigram)
            if match:
                w2 = match.group(1)
                pairs[w2] = self.pair_probability(w1, w2)
        c = 0
        for x in pairs:
            c += pairs[x]
        for x in pairs:
            pairs[x] = pairs[x] / c
        seq_words = list(pairs.keys())
        seq_words.sort(key=lambda w: pairs[w], reverse=True)
        pos = random.random()
        delta = 0
        for w in seq_words:
            if pos < delta:
                return w
            delta += pairs[w]
        return seq_words[-1]


lm = LanguageModel("leme.conll")
print("Language Model Ready")

sentence = ["<s>"]
for i in range(0, 15):
    sentence.append(lm.next_word(sentence[-1]))
print(sentence)


# p1 = lm.seq_probability(["leia", "atentamente", "este", "folheto"])
# print(f"P1: {p1}")
#
# p2 = lm.seq_probability(["atentamente", "este", "folheto", "leia"])
# print(f"P2: {p2}")
#
# p3 = lm.seq_probability(["atentamente", "atentamente","atentamente","atentamente"])
# print(f"P3: {p3}")
#
# p4 = lm.seq_probability(["dores", "de", "cabeça"])
# print(f"P4: {p4}")