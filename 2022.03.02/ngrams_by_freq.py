# ngrams_by_freq
import re

from utils import load_conll_words, load_word_set
text = load_conll_words("leme.conll")
stop_words = load_word_set("stopword.txt")


def valid(word):
    word_lc = word.lower()
    return word_lc not in stop_words and\
        re.fullmatch(r'\w+', word_lc) and\
        not re.match(r'\d', word_lc)


N = 2
ngrams_freq = {}

for sent in text:
    n = len(sent) - N + 1
    for i in range(0, n):
        if valid(sent[i]) and valid(sent[i+N-1]):
            seq = "_".join(sent[i:i+N]).lower()
            ngrams_freq[seq] = ngrams_freq.get(seq, 0) + 1


tokens = list(ngrams_freq.keys())
tokens.sort(key=lambda x: ngrams_freq[x], reverse=True)
print(*tokens[:100], sep="\n")

