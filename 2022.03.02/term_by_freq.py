# term_by_freq
import re
from utils import load_conll_words, load_word_set

text = load_conll_words("leme.conll")
stop_words = load_word_set("formas.cetempublico.txt")

word_freq = {}
for sentence in text:
    for token in sentence:
        lc_token = str.lower(token)
        if re.fullmatch(r'\w+', token) and \
            not re.match(r'\d', token) and \
                lc_token not in stop_words:
            word_freq[lc_token] = word_freq.get(lc_token, 0) + 1

tokens = list(word_freq.keys())
tokens.sort(key=lambda x: word_freq[x], reverse=True)

print(*tokens[:50], sep="\n")

