#!/usr/bin/env python3

import stanza
import re
import sys

#   H2O  <=   H<sub>2</sub>O
# foo<bar>zbr   => foozbr

if len(sys.argv) != 2:
    print("Supply source filename")
    exit(1)

filename = sys.argv[1]

stanza.download('pt')
with open(filename, "r") as fh:  # LEaflets of MEdicine
    content = fh.read()

content = re.sub(r'<[^>]+>', " ", content)

nlp = stanza.Pipeline(lang='pt', processors='tokenize,pos,lemma', use_gpu=True)
doc = nlp(content)

for sentence in doc.sentences:
    print("<s>")
    for token in sentence.words:
        print("%s\t%s\t%s" % (token.text, token.lemma, token.upos))
    print("</s>")
