# utils.py

def load_word_set(filename):
    word_set = set()
    with open(filename, "r") as fh:
        for line in fh:
            word_set.add(str.lower(line.rstrip()))
    return word_set


def load_conll_words(filename):
    text = []
    sentence = []
    with open(filename, "r") as fh:
        for line in fh:
            line = line.rstrip()
            (token, *_) = line.split("\t")    # token = line.split("\t")[0]
            sentence.append(token)
            if token == "</s>":
                text.append(sentence)
                sentence = []
    return text

