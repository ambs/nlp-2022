# ngrams_by_freq
import pprint
import re
import math
from utils import load_conll_words, load_word_set
text = load_conll_words("leme.conll")
stop_words = load_word_set("stopword.txt")


def valid(word):
    word_lc = word.lower()
    return word_lc not in stop_words and\
        re.fullmatch(r'\w+', word_lc) and\
        not re.match(r'\d', word_lc)


N = 2
ngrams_freq = {}
word_freq = {}

for sent in text:
    n = len(sent) - N + 1
    for w in sent:
        word = w.lower()
        word_freq[word] = word_freq.get(word, 0) + 1

    for i in range(0, n):
        if valid(sent[i]) and valid(sent[i+N-1]):
            seq = "|".join(sent[i:i+N]).lower()
            ngrams_freq[seq] = ngrams_freq.get(seq, 0) + 1


jaccard = {}
for bigram in ngrams_freq.keys():
    if ngrams_freq[bigram] < 2:
        continue
    (w1, w2) = bigram.split("|")
    J = ngrams_freq[bigram]/(word_freq[w1] + word_freq[w2] - ngrams_freq[bigram])
    jaccard[bigram] = J


tokens = list(jaccard.keys())
tokens.sort(key=lambda x: jaccard[x], reverse=True)
for token in tokens:
    print(f"{token}   # {ngrams_freq[token]}  Jaccard {jaccard[token]}")


