# word_matrix.py
from glob import glob
from utils import load_conll_words, load_word_set, cosine
import numpy as np

vocabulary = list(load_word_set("vocabulary.txt"))
voc2idx = {}
for idx, word in enumerate(vocabulary):
    voc2idx[word] = idx

window_size = 3
matrix = {w: np.zeros(len(vocabulary)) for w in vocabulary}

for file in glob("docs/*.conll"):
    text = load_conll_words(file, lowercase=True)
    for sentence in text:
        for word_pos, word in enumerate(sentence):
            low = max(word_pos - window_size, 0)
            high = min(word_pos + window_size, len(sentence) - 1)
            for win_pos, context in enumerate(sentence[low:high+1], start=low):
                if win_pos != word_pos:
                    if word in vocabulary and context in vocabulary:
                        matrix[word][voc2idx[context]] += 1

escitalopram = matrix["escitalopram"]
paroxetina = matrix["paroxetina"]
intestino = matrix["intestino"]
ibuprofeno = matrix["ibuprofeno"]
paracetamol = matrix["paracetamol"]

print(f"Paroxetina vs Escitalopram {cosine(escitalopram,paroxetina)}")
print(f"Intestino vs Paroxetina {cosine(intestino,paroxetina)}")
print(f"Paracetamol vs Ibuprofeno {cosine(paracetamol,ibuprofeno)}")

somatorio = 0
somatorio_por_palavra = {}
somatorio_por_contexto = np.zeros(len(vocabulary))

for x in vocabulary:
    somatorio_por_palavra[x] = np.sum(matrix[x])
    somatorio += somatorio_por_palavra[x]
    somatorio_por_contexto += matrix[x]


def pmi(word, context):
    word_context_count = matrix[word][voc2idx[context]]
    p_word_context = word_context_count / somatorio
    p_word = word_context_count / somatorio_por_palavra[word]
    p_context = word_context_count / somatorio_por_contexto[voc2idx[context]]
    return np.log2(p_word_context / (p_context * p_word))


def ppmi(word, context):
    return max(0, pmi(word, context))


for word in vocabulary:
    for idx, context in enumerate(vocabulary):
        matrix[word][idx] = ppmi(word, context)




escitalopram = matrix["escitalopram"]
paroxetina = matrix["paroxetina"]
intestino = matrix["intestino"]
ibuprofeno = matrix["ibuprofeno"]
paracetamol = matrix["paracetamol"]

print(f"Paroxetina vs Escitalopram {cosine(escitalopram,paroxetina)}")
print(f"Intestino vs Paroxetina {cosine(intestino,paroxetina)}")
print(f"Paracetamol vs Ibuprofeno {cosine(paracetamol,ibuprofeno)}")

