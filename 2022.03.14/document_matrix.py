# document_matrix.py
from glob import glob
from utils import load_conll_words, load_word_set, cosine
import numpy as np


def tf_idf(count, N, df_word):
    return np.log10(count + 1) * np.log10(N / df_word)


def count(tf, word, document):
    if word in tf:
        if document in tf[word]:
            return tf[word][document]
    return 0


def df(tf, word):
    if word in tf:
        return len(tf[word].keys())
    return 0


vocabulary = list(load_word_set("vocabulary.txt"))
voc2idx = {}
for idx, word in enumerate(vocabulary):
    voc2idx[word] = idx

matrix = {}
tf = {}
documents = glob("docs/*.conll")
for file in documents:
    text = load_conll_words(file, lowercase=True)
    count_vector = np.zeros(len(vocabulary))
    for sentence in text:
        for word in sentence:
            if word in vocabulary:
                count_vector[voc2idx[word]] += 1
                if word in tf:
                    if file in tf[word]:
                        tf[word][file] += 1
                    else:
                        tf[word][file] = 1
                else:
                    tf[word] = {file: 1}
    matrix[file] = count_vector



escitalopram = matrix["docs/escitalopram.conll"]
paroxetina = matrix["docs/paroxetina.conll"]
ibuprofeno = matrix["docs/ibuprofeno.conll"]
paracetamol = matrix["docs/paracetamol.conll"]
paracetamol_cafeina = matrix["docs/paracetamol_cafeina.conll"]

print(f"Paroxetina vs Escitalopram {cosine(escitalopram,paroxetina)}")
print(f"Ibuprofeno vs Paracetamol {cosine(ibuprofeno, paracetamol)}")
print(f"Paracetamol/Cafeina vs Paracetamol {cosine(paracetamol_cafeina, paracetamol)}")

for document in matrix:
    for word in vocabulary:
        matrix[document][voc2idx[word]] = tf_idf(count(tf, word, document), len(documents), df(tf, word))


escitalopram = matrix["docs/escitalopram.conll"]
paroxetina = matrix["docs/paroxetina.conll"]
ibuprofeno = matrix["docs/ibuprofeno.conll"]
paracetamol = matrix["docs/paracetamol.conll"]
paracetamol_cafeina = matrix["docs/paracetamol_cafeina.conll"]

print(f"Paroxetina vs Escitalopram {cosine(escitalopram,paroxetina)}")
print(f"Ibuprofeno vs Paracetamol {cosine(ibuprofeno, paracetamol)}")
print(f"Paracetamol/Cafeina vs Paracetamol {cosine(paracetamol_cafeina, paracetamol)}")
