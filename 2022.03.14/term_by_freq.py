# term_by_freq
import re
from utils import load_conll_words, load_word_set
from glob import glob

threshold = 10 # minimum occ count

word_freq = {}
for file in glob("docs/*.conll"):
    text = load_conll_words(file, lowercase=True)

    for sentence in text:
        for token in sentence:
            lc_token = str.lower(token)
            if re.fullmatch(r'\w+', token) and \
                not re.match(r'\d', token):
                word_freq[lc_token] = word_freq.get(lc_token, 0) + 1

with open("vocabulary.txt", "w+") as fh:
    for word in word_freq:
        if word_freq[word] >= threshold:
            print(word, file=fh)

