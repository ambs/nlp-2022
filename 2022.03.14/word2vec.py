# word2vec.py
from gensim.models import Word2Vec
from utils import load_conll_words, cosine

sentences = load_conll_words("../2022.03.03/leme.conll", lowercase=True)
model = Word2Vec(sentences=sentences, vector_size=300, window=5, min_count=10, workers=4)
model.save("leme.model")

## KeyedVectors
wv = model.wv
wv.save("leme.kv")
