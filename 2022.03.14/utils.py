# utils.py
import numpy as np
from numpy.linalg import norm

def cosine(u, v):
    dot_product = np.dot(u, v)
    u_norm = norm(u)
    v_norm = norm(v)
    return dot_product / (u_norm * v_norm)


def load_word_set(filename):
    word_set = set()
    with open(filename, "r") as fh:
        for line in fh:
            word_set.add(str.lower(line.rstrip()))
    return word_set


def load_conll_words(filename, lowercase=False, just_words=True):
    text = []
    sentence = []
    with open(filename, "r") as fh:
        for line in fh:
            line = line.rstrip()

            if just_words:
                (token, *_) = line.split("\t")    # token = line.split("\t")[0]
            else:
                # line = "comprimidos	comprimir	NOUN"
                # line = "<s>"
                tokens = line.split("\t")
                (token, lema, pos) = tokens if len(tokens) == 3 else (tokens[0], tokens[0], tokens[0])

            if lowercase:
                token = token.lower()

            if just_words:
                sentence.append(token)
            else:
                sentence.append((token, lema, pos))

            if token == "</s>":
                text.append(sentence)
                sentence = []
    return text

