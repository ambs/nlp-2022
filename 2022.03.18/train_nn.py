import json

from keras import Input
from keras.layers import Embedding, Flatten, Dense, Softmax
from keras.models import Sequential
from keras.utils.data_utils import Sequence
from math import ceil
import numpy as np

from utils import load_conll_words


class MyGenerator(Sequence):
    def __init__(self, sentences, n=3, batch_size=64, threshold=10):
        self.sentences = sentences
        self.n = n
        self.batch_size = batch_size
        self._add_padding()
        self.vocabulary = self._compute_vocabulary(threshold)
        self.word_to_idx = {w: pos for pos, w in enumerate(self.vocabulary)}
        self.example_count = self._count_examples()
        self.current_sentence = 0
        self.current_token = 0

    def __getitem__(self, index):
        if index == 0:
            self.current_token = 0
            self.current_sentence = 0
        batch_x = []
        batch_y = []
        while True:
            x_indexes = [self._get_word_idx(word) for word in
                         self.sentences[self.current_sentence]
                         [self.current_token: self.current_token + self.n]]
            y_onehot = self._get_word_onehot(self.sentences[self.current_sentence]
                                             [self.current_token + self.n])
            batch_x.append(np.array(x_indexes))
            batch_y.append(y_onehot)
            if self.current_token < len(self.sentences[self.current_sentence]) - self.n - 1:
                self.current_token += 1
            else:
                self.current_token = 0
                if self.current_sentence < len(self.sentences) - 1:
                    self.current_sentence += 1
                else:
                    break
            if len(batch_x) == self.batch_size:
                break

        return np.array(batch_x), np.array(batch_y)

    def __len__(self):
        return ceil(self.example_count / self.batch_size)

    def _get_word(self, word):
        return word if word in self.vocabulary else "<unk>"

    def _get_word_onehot(self, word):
        idx = self._get_word_idx(word)
        vector = np.zeros(len(self.vocabulary))
        vector[idx] = 1
        return vector

    def _get_word_idx(self, word):
        w = self._get_word(word)
        return self.word_to_idx[w]

    def _count_examples(self):
        count = 0
        for s in self.sentences:
            count += len(s) - self.n
        return count

    def _compute_vocabulary(self, threshold):
        freqs = {}
        for s in self.sentences:
            for w in s:
                freqs[w] = freqs.get(w, 0) + 1
        voc = [w for w in freqs if freqs[w] > threshold] + ["<unk>"]
        np.random.shuffle(voc)
        return voc

    def _add_padding(self):
        prefix = ["<s>"] * (self.n - 1)
        self.sentences = [prefix + s for s in self.sentences]

    def save_vocabulary(self, filename):
        with open(filename, "w+") as fh:
            json.dump(self.word_to_idx, fh)


text = load_conll_words("leme.conll", just_words=True, lowercase=True)
generator = MyGenerator(text, n=2)
generator.save_vocabulary("vocabulary.json")

model = Sequential()
model.add(Input(shape=(generator.n,)))  # (3,1)
model.add(Embedding(input_dim=len(generator.vocabulary),
                    input_length=generator.n,
                    output_dim=100))  # Embedding size
model.add(Flatten())
model.add(Dense(len(generator.vocabulary)))
model.add(Softmax())
model.compile(optimizer="adam",
              metrics=["accuracy"],
              # run_eagerly=True,
              loss="binary_crossentropy")
model.fit(x=generator, verbose=True, batch_size=generator.batch_size, epochs=3)
model.save("model.save")
