import json

import numpy as np
from keras import Model
from keras.models import load_model

with open("vocabulary.json", "r") as f:
    word2index = json.loads(f.read())

index2word = {word2index[w]: w for w in word2index.keys()}

model = load_model("model.save")
model.summary()

sentence = [word2index[w] for w in ['<s>', '<s>', '<s>']]
for i in range(10):
    x = sentence[-2:]
    print(x)
    y = model.predict(np.array([x]))
    pos = np.argmax(y[0])
    print(index2word[pos], y[0][pos])
    sentence.append(pos)




