# fasttext.py

from gensim.models import FastText
from utils import load_conll_words, cosine

sentences = load_conll_words("../2022.03.03/leme.conll", lowercase=True)
model = FastText(vector_size=300, window=5, min_count=10, workers=4)
model.build_vocab(corpus_iterable=sentences)
model.train(corpus_iterable=sentences, total_examples=len(sentences), epochs=10)
wv = model.wv
wv.save("leme-ft.kv")
