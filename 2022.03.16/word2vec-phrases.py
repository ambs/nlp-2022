# word2vec.py
from gensim.models import Word2Vec
from gensim.models import Phrases
from utils import load_conll_words

sentences = load_conll_words("../2022.03.03/leme.conll", lowercase=True)

phrases = Phrases(sentences)

model = Word2Vec(sentences=phrases[sentences], vector_size=300, window=5, min_count=10, workers=4)

## KeyedVectors
wv = model.wv
wv.save("leme-mw.kv")
