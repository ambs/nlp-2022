# create_twitter_embeddings.py
import nltk
from utils import process_tweet
from gensim.models import Word2Vec
from nltk.corpus import twitter_samples

# 1. download stopwords and tweets
nltk.download("stopwords")
nltk.download("twitter_samples")

# 2. create twitter corpus
positive = twitter_samples.strings("positive_tweets.json")
negative = twitter_samples.strings("negative_tweets.json")

# 3. join tweets
tweets = positive + negative
corpus = map(process_tweet, tweets)

# 4. train word2vec
model = Word2Vec(sentences=corpus, vector_size=100, window=5, min_count=3, workers=4)
wv = model.wv
wv.save("twitter.wv")







