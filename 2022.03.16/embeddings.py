# Word2Vec => wv
# FastText => wv
from gensim.models import KeyedVectors
from utils import cosine
from typing import cast

model: KeyedVectors = cast(KeyedVectors, KeyedVectors.load("leme-mw.kv"))

# Palavras proximas (semanticamente)
near = model.similar_by_word("intestino")
print(near)
print(cosine(model["intestino"], model["esófago"]))
print(cosine(model["intestino"], model["inflamação"]))

# Analogia
result = model.most_similar(
    positive=["coração", "avc"],
    negative=["cérebro"]
)
print(result)

# Doesn't match
print(model.doesnt_match(["cérebro", "intestino", "coração", "febre"]))
print(model.doesnt_match(["cérebro", "escitalopram", "sertralina", "olmesartan"]))

# Mover's distance
print(model.wmdistance(["pode","provocar","cólicas"], ["pode", "provocar", "rim"]))
print(model.wmdistance(["pode","provocar","cólicas"], ["pode", "provocar", "morte"]))
print(model.wmdistance(["pode","provocar","cólicas"], ["produz", "cólicas"]))


