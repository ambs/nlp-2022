# train_sentiment_nn.py
from nltk.corpus import twitter_samples
from gensim.models import KeyedVectors
import numpy as np
from typing import cast
from keras.models import Sequential
from keras.layers import Dense
from utils import process_tweet


def tweet_to_embedding(embedding, tweet):
    word_count = 0
    tweet_embedding = np.zeros(len(embedding[0]))
    for word in tweet:
        if word in embedding:
            word_count += 1
            tweet_embedding += embedding[word]
    if word_count > 0:
        tweet_embedding = tweet_embedding / word_count
    return tweet_embedding


embeddings = cast(KeyedVectors, KeyedVectors.load("twitter.wv"))

# -- Codigo Aula --
# positive = [tweet_to_embedding(embeddings, x) for x in twitter_samples.strings("positive_tweets.json")]
# negative = [tweet_to_embedding(embeddings, x) for x in twitter_samples.strings("negative_tweets.json")]
# -- Codigo COrreto --
positive = [tweet_to_embedding(embeddings, process_tweet(x)) for x in twitter_samples.strings("positive_tweets.json")]
negative = [tweet_to_embedding(embeddings, process_tweet(x)) for x in twitter_samples.strings("negative_tweets.json")]
corpus_size = len(positive)

print(len(positive), len(negative))
train_X = np.array(positive[:4000] + negative[:4000])
test_X = np.array(positive[4000:] + negative[4000:])

train_Y = np.append(np.ones(4000), np.zeros(4000))
# train_Y = train_Y.reshape((len(train_Y), 1))
test_Y = np.append(np.ones(1000), np.zeros(1000))
test_Y = test_Y.reshape((len(test_Y), 1))

# train_X = np.array([tweet_to_embedding(embeddings, x) for x in train_X])
# test_X = np.array([tweet_to_embedding(embeddings, x) for x in test_X])
print(train_X.shape, train_Y.shape, test_X.shape, test_Y.shape)

# Neural Network
nn_model = Sequential()
nn_model.add(Dense(200, activation='relu', input_dim=100))
nn_model.add(Dense(1, activation='sigmoid', input_dim=200))
nn_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
nn_model.summary()

nn_model.fit(train_X, train_Y, batch_size=32, epochs=4, verbose=True)

predicted = nn_model.predict(test_X) >= 0.5
wrong = np.sum(np.abs(predicted - test_Y))
correct = len(test_Y) - wrong
print(correct)
print("Accuracy: %.4f" % (correct/len(test_Y)))











